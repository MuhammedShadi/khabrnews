class NewUser {
  String firstName;
  String lastName;
  String username;
  String email;
  String password;

  NewUser(
    this.firstName,
    this.lastName,
    this.username,
    this.email,
    this.password,
  );
}
